import { cloneDeep } from 'lodash';

const getNestedChildren = (arr, parent) => {
    let root = 'root';
    parent = parent || root;
    let out = [];
    for (let i in arr) {
        arr[i].parent = arr[i].parent || root;
        if (arr[i].parent === parent) {
            let children = getNestedChildren(arr, arr[i].id);

            if (children.length) {
                arr[i].children = children;
            }
            out.push(arr[i]);
        }
    }
    return out;
};

export const parseCategory = (categories) => {
    let arr = cloneDeep(categories); //TODO: refactor
    return getNestedChildren(arr);
};

export const guid = () => {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

export const countProgress = (todos) => {
    let all = todos.length;
    let done = todos.filter(todo => todo.isDone).length;
    return Math.floor(( done / all) * 100);
};

