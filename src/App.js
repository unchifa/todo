import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './components/Pages/Home';
import TaskEdit from './components/Pages/TaskEdit';

class App extends Component {

    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact component={ Home }/>
                    <Route path='/category/:id?' component={ Home }/>
                    <Route path='/edit/:id?' component={ TaskEdit }/>
                    <Route render={ () => <h1>Page not found</h1>}/>
                </Switch>
            </Router>
        );
    }
}

export default App;
