import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import configureStore from './store/configureStore';
import state from './store/initialState';
import { loadCategories } from './store/actions';
import { loadTodos } from './store/actions';

const store = configureStore(state);
store.dispatch(loadCategories());
store.dispatch(loadTodos());

ReactDOM.render(
    <Provider store={ store }>
        <App />
    </Provider>,
    document.getElementById('root')
);
