import delay from './delay';
import { guid } from '../lib/todoHelpers';

const todos = [
    { 'id': '1', 'text': 'To-Do Item #1', 'isDone': false, category: '1', description: 'Description' },
    { 'id': '2', 'text': 'To-Do Item #2', 'isDone': true, category: '1', description: 'Description' },
    { 'id': '3', 'text': 'To-Do Item #3', 'isDone': true, category: '1', description: 'Description' },
    { 'id': '4', 'text': 'To-Do Item #4', 'isDone': true, category: '1', description: 'Description' },
    { 'id': '5', 'text': 'To-Do Item #5', 'isDone': true, category: '1', description: 'Description' }
];

const generateId = () => {
    return guid();
};

class TodoApi {
    static getAllTodos() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(Object.assign([], todos));
            }, delay);
        });
    }

    static saveTodo(todo) {
        todo = Object.assign({}, todo);
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const minTextLength = 1;
                if (todo.text.length < minTextLength) {
                    reject(`Text must be at least ${minTextLength} characters.`);
                }

                if (todo.id) {
                    const existingId = todos.findIndex(a => a.id === todo.id);
                    todos.splice(existingId, 1, todo);
                } else {
                    todo.id = generateId();
                    todos.push(todo);
                }

                resolve(todo);
            }, delay);
        });
    }
}

export default TodoApi;