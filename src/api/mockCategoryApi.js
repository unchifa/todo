import delay from './delay';
import { guid } from '../lib/todoHelpers';

const categories = [
    { id: '1', text: 'Category 1 from API' },
    { id: '2', text: 'Category 2' },
    { id: '3', text: 'Category 3' },
    { id: '4', text: 'Category 4' },
    { id: '5', text: 'Category 5' },
    { id: '6', text: 'Category 6' },
    { id: '7', text: 'Category 3 1', parent: '3' },
    { id: '8', text: 'Category 3 2', parent: '3' },
    { id: '9', text: 'Category 3 3', parent: '3' },
    { id: '10', text: 'Category 3 1 1', parent: '7' },
    { id: '11', text: 'Category 3 1 2', parent: '7' }
];

const generateId = () => {
    return guid();
};

class CategoryApi {
    static getAllCategories() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(Object.assign([], categories));
            }, delay);
        });
    }

    static saveCategory(category) {
        category = Object.assign({}, category); // to avoid manipulating object passed in.
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const minTextLength = 1;
                if (category.text.length < minTextLength) {
                    reject(`Text must be at least ${minTextLength} characters.`);
                }

                if (category.id) {
                    const existingId = categories.findIndex(a => a.id === category.id);
                    categories.splice(existingId, 1, category);
                } else {
                    category.id = generateId();
                    categories.push(category);
                }

                resolve(category);
            }, delay);
        });
    }

    static deleteCategory(categoryId) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const indexOfCategoryToDelete = categories.findIndex(category => {
                    return category.id === categoryId;
                });
                categories.splice(indexOfCategoryToDelete, 1);
                resolve();
            }, delay);
        });
    }
}

export default CategoryApi;