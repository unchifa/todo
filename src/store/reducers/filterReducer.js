import { TOGGLE_SHOW_DONE, CHANGE_FILTER } from '../actions';

const filterReducer = (state = {}, action) => {
    switch (action.type) {
        case TOGGLE_SHOW_DONE:
            return { ...state, showDone: !state.showDone };
        case CHANGE_FILTER:
            let query = action.query;
            return { ...state, query };
        default:
            return state;
    }
};

export default filterReducer;