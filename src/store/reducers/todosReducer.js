import { CREATE_TODO_SUCCESS, UPDATE_TODO_SUCCESS, LOAD_TODOS_SUCCESS } from '../actions';
import { undoable } from './undoable';

const todosReducer = (state = [], action) => {
    switch (action.type) {
        case LOAD_TODOS_SUCCESS:
            return action.todos;
        case CREATE_TODO_SUCCESS:
            return [ action.todo, ...state ];
        case UPDATE_TODO_SUCCESS:
            return state.map(todo => {
                if (todo.id === action.todo.id) {
                    return { ...action.todo };
                }
                return todo;
            });
        default:
            return state;
    }
};
export default undoable(todosReducer);