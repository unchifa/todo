import {
    DELETE_CATEGORY_SUCCESS, LOAD_CATEGORIES_SUCCESS, CREATE_CATEGORY_SUCCESS, UPDATE_CATEGORY_SUCCESS
} from '../actions';

const categoriesReducer = (state = [], action) => {
    switch (action.type) {
        case LOAD_CATEGORIES_SUCCESS:
            return action.categories;
        case CREATE_CATEGORY_SUCCESS:
            return [{ ...action.category }, ...state];
        case UPDATE_CATEGORY_SUCCESS:
            let text = action.category.text;
            let id = action.category.id;
            return state.map(category => category.id === id ? { ...category, text } : category);
        case DELETE_CATEGORY_SUCCESS:
            //TODO: delete children
            return state.filter(category => category.id !== action.id);
        default:
            return state;
    }
};

export default categoriesReducer;