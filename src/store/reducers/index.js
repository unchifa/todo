import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import todos from './todosReducer';
import categories from './categoriesReducer';
import filter from './filterReducer';

const rootReducer = combineReducers({
    todos,
    categories,
    filter,
    form
});

export default rootReducer;