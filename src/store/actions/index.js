import categoryApi from '../../api/mockCategoryApi';
import todoApi from '../../api/mockTodoApi';

export const DELETE_CATEGORY_SUCCESS = 'DELETE_CATEGORY_SUCCESS';
export const LOAD_CATEGORIES_SUCCESS = 'LOAD_CATEGORIES_SUCCESS';
export const CREATE_CATEGORY_SUCCESS = 'CREATE_CATEGORY_SUCCESS';
export const UPDATE_CATEGORY_SUCCESS = 'UPDATE_CATEGORY_SUCCESS';

export const LOAD_TODOS_SUCCESS = 'LOAD_TODOS_SUCCESS';
export const CREATE_TODO_SUCCESS = 'CREATE_TODO_SUCCESS';
export const UPDATE_TODO_SUCCESS = 'UPDATE_TODO_SUCCESS';

export const TOGGLE_SHOW_DONE = 'TOGGLE_SHOW_DONE';
export const CHANGE_FILTER = 'CHANGE_FILTER';

export const UNDO = 'UNDO';
export const REDO = 'REDO';


// UNDO/REDO
export const undo = () => {
    return { type: UNDO }
};

export const redo = () => {
    return { type: REDO }
};

// TODOS
export const loadTodosSuccess = (todos) => {
    return { type: LOAD_TODOS_SUCCESS, todos }
};

export const loadTodos = () => {
    return function (dispatch) {
        return todoApi.getAllTodos().then(todos => {
            dispatch(loadTodosSuccess(todos));
        }).catch(error => console.error(error));
    };
};

export const createTodoSuccess = (todo) => {
    return { type: CREATE_TODO_SUCCESS, todo }
};

export const updateTodoSuccess = (todo) => {
    return { type: UPDATE_TODO_SUCCESS, todo }
};

export const saveTodo = (todo) => {
    return function (dispatch) {
        let isUpdate = !!todo.id;
        return todoApi.saveTodo(todo).then(todo => {
            isUpdate
                ? dispatch(updateTodoSuccess(todo))
                : dispatch(createTodoSuccess(todo));
        })
    }
};

// CATEGORIES
export const loadCategoriesSuccess = (categories) => {
    return { type: LOAD_CATEGORIES_SUCCESS, categories }
};

export const loadCategories = () => {
    return function (dispatch) {
        return categoryApi.getAllCategories().then(categories => {
            dispatch(loadCategoriesSuccess(categories));
        }).catch(error => console.error(error));
    };
};

export const createCategorySuccess = (category) => {
    return { type: CREATE_CATEGORY_SUCCESS, category }
};

export const updateCategorySuccess = (category) => {
    return { type: UPDATE_CATEGORY_SUCCESS, category }
};

export const saveCategory = (category) => {
    return function (dispatch, getState) {
        let isUpdate = !!category.id;
        return categoryApi.saveCategory(category).then(category => {
            isUpdate
                ? dispatch(updateCategorySuccess(category))
                : dispatch(createCategorySuccess(category));
        })
    }
};

export const deleteCategorySuccess = (id) => {
    return { type: DELETE_CATEGORY_SUCCESS, id };
};

export const deleteCategory = (id) => {
    return function (dispatch) {
        return categoryApi.deleteCategory(id).then(() => {
            dispatch(deleteCategorySuccess(id));
        }).catch(error => console.error(error));
    };
};

// FILTER
export const toggleShowDone = () => {
    return { type: TOGGLE_SHOW_DONE };
};

export const changeFilter = (query) => {
    return { type: CHANGE_FILTER, query };
};
