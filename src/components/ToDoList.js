import React, { Component } from 'react';
import MdEdit from 'react-icons/lib/md/edit';
import { Link } from 'react-router-dom';

class ToDoList extends Component {
    render() {
        return (
            <div className="todo-list">
                { this.props.items.map(item =>
                    <div className="panel panel-default" key={ item.id }>
                        <div className="panel-body">
                            <div className="row">
                                <span className="col-xs-1">
                                    <input type="checkbox"
                                           onChange={() =>  this.props.onToggle({ ...item, isDone: !item.isDone }) }
                                           checked={ item.isDone }/>
                                </span>
                                <span className="col-xs-9">{ item.text }</span>
                                <span className="col-xs-2"><Link to={`/edit/${ item.id }`}><MdEdit /></Link></span>
                            </div>
                        </div>
                    </div>
                ) }
            </div>
        );
    }
}

export default ToDoList;