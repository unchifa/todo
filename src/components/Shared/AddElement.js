import React from 'react';

export const AddElement = (props) => (

    <div className="form-inline">
        <div className="form-group">
            <input type="text"
                   onChange={props.onChange}
                   value={props.text}
                   className="form-control"
                   placeholder={ props.placeholder }/>
        </div>
        <button onClick={props.onAdd} className="btn btn-default">Add</button>
    </div>
);