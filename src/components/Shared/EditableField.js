import React, { Component } from 'react';
import MdEdit from 'react-icons/lib/md/edit';
import MdSave from 'react-icons/lib/md/save';
import Button from 'react-bootstrap/lib/Button';
import { Link } from 'react-router-dom';
import './EditableField.css';

class EditableField extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            isEditMode: this.props.createNew ? true : false,
            text: this.props.text
        };
    }

    toggleEditing() {
        this.setState({
            isEditMode: !this.state.isEditMode
        });
    }

    handleInput() {
        let text = this.state.text;
        if (text) {
            this.toggleEditing();
            this.props.onSave(text);
        }
    }

    renderFiled() {
        return (
            <span className="editable-field">
                    <input
                        className="form-control"
                        type="text"
                        onChange={ (e) => this.setState({ text: e.target.value })}
                        value={ this.state.text }/>
                    <Button onClick={ () => this.handleInput()  } bsStyle="link"><MdSave /></Button>
            </span>
        );
    }

    renderText() {
        return (
            <span>
                { this.props.link ? <Link to={ this.props.link }>{ this.props.text }</Link> : <span>{ this.props.text }</span>  }
                <Button onClick={ () => this.toggleEditing()  } bsStyle="link"><MdEdit /></Button>
            </span>

        );
    }

    render() {
        return this.state.isEditMode ? this.renderFiled() : this.renderText();
    }
}

export default EditableField;