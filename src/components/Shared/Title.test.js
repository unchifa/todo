import React from 'react';
import { shallow } from 'enzyme';
import { Title } from './Title';

describe('<Title />', () => {
    it('renders title', () => {
        const wrapper = shallow(<Title text="To-Do List"/>);
        expect(wrapper.find('h1').text()).toBe('To-Do List');
    })
});