import React from 'react';
import { Search } from './Search';
import { shallow } from 'enzyme';

const props = { onChange: () => {}, onClear: () => {}, onToggle: () => {}, query: 'filter', checked: true }
const search = <Search  { ...props } />

describe('<Search />', () => {
    it('renders inputs', () => {
        const wrapper = shallow(search);
        expect(wrapper.find('input').length).toBe(2);
    });

    it('renders search input and checkbox', () => {
        const wrapper = shallow(search);
        expect(wrapper.find('input').first().props().type).toBe('checkbox');
        expect(wrapper.find('input').last().props().type).toBe('search');
    })
});