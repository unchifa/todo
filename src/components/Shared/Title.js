import React from 'react';

export const Title = (props) => (
    <h1>{ props.text }</h1>
);