import React from 'react';
import MdClear from 'react-icons/lib/md/clear';
import Button from 'react-bootstrap/lib/Button';

export const Search = (props) => (
    <div className="form-inline">
        <div className="checkbox">
            <label>
                <input
                    checked={ props.checked }
                    onChange={ props.onToggle }
                    type="checkbox" name="show-done"/> Show done
            </label>
        </div>
        <div className="form-group">
            <label className="sr-only" htmlFor="seacth-input">Search</label>
            <input value={ props.query }
                   onChange={ props.onChange }
                   type="search" className="form-control" id="seacth-input" placeholder="Search"/>
        </div>
        <Button onClick={ props.onClear }><MdClear /></Button>
    </div>
); 