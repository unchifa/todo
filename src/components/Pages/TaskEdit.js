import React, { Component } from 'react';
import { Title } from './../Shared';
import CategoryList from '../CategoryList/CategoryList';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import { connect } from 'react-redux';
import { parseCategory } from '../../lib/todoHelpers';
import EditForm from '../EditForm';
import { saveTodo } from '../../store/actions';

class TaskEdit extends Component {
    state = {
        categoryId: null
    };

    onUpdate(data) {
        // TODO: check if todo is changed
        let id = this.props.todo.id;
        let category = this.state.categoryId || this.props.todo.category;
        this.props.saveTodo({ ...data, id, category });
        this.props.goBack();
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col xs={12} sm={6}><Title text="To-Do List"/></Col>
                    <Col xs={12} sm={6} className="text-right"></Col>
                </Row>

                <Row>
                    <Col xs={12} sm={4}><CategoryList
                        editMode={ true }
                        onMove={ (categoryId) => {
                            this.setState({ categoryId });
                        }}
                        selectedCategory={ this.state.categoryId }
                        categories={ this.props.categories }/>
                    </Col>
                    <Col xs={12} sm={8}>
                        { this.props.todo
                            ? <EditForm
                                todo={ this.props.todo }
                                onSubmit={data => this.onUpdate(data)}
                                onCancel={e => {
                                    e.preventDefault();
                                    this.props.goBack();
                                }}/>
                            : <h2>ToDo not found</h2>
                        }
                    </Col>

                </Row>
            </Grid>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        todo: state.todos.present.find((todo) => todo.id === ownProps.match.params.id),
        categories: parseCategory(state.categories),
        goBack: ownProps.history.goBack
    };
}

function mapDispatchToProps(dispatch) {
    return {
        saveTodo: todo => dispatch(saveTodo(todo))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskEdit);