import React, { Component } from 'react';
import { Title, Search, AddElement } from './../Shared';
import UndoRedo from '../UndoRedo';
import CategoryList from '../CategoryList/CategoryList';
import ToDoList from './../ToDoList';
import { parseCategory, countProgress } from '../../lib/todoHelpers';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import  PropTypes  from 'prop-types';
import { connect } from 'react-redux';
import {
    saveCategory,
    saveTodo,
    deleteCategory,
    toggleShowDone,
    changeFilter
} from '../../store/actions';
import './Home.css';

class Home extends Component {

    state = { todoTextValue: '', categoryTextValue: '' };

    onAddTodo() {
        let text = this.state.todoTextValue;
        let category = this.getCurrentCategoryId();
        if (text) {
            this.props.saveTodo({ text, category });
        }
        this.setState({ todoTextValue: '' });
    };

    onCategoryAdd() {
        let text = this.state.categoryTextValue;
        if (text) {
            this.props.saveCategory({ text });
        }
        this.setState({ categoryTextValue: '' });
    };

    onCategoryDelete(id) {
        if (id) {
            this.props.deleteCategory(id);
        }
    };

    onFilterChange(query) {
        const location = this.context.router.history.location;
        const search = query ? `filter=${ query }` : '';
        this.context.router.history.push({...location, search  })
        this.props.changeFilter(query);
    }

    getCurrentCategoryId() {
        return this.props.match.params.id;
    }

    isListVisible() {
        return this.getCurrentCategoryId();
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col xs={12} sm={6}><Title text="To-Do List"/></Col>
                    <Col xs={12} sm={6} className="text-right search-block">
                        <Search
                            onChange={ (event) => this.onFilterChange(event.target.value) }
                            onClear={ () => this.onFilterChange('') }
                            onToggle={ () => this.props.toggleShowDone() }
                            query={ this.props.filter.query }
                            checked={ this.props.filter.showDone }/>
                    </Col>
                </Row>

                <Row>
                    <Col xs={12}>
                        <ProgressBar now={ this.props.progress } label={`${ this.props.progress }%`} srOnly/>
                    </Col>
                </Row>

                <Row className="add-elements-block">
                    <Col xs={12} sm={4}>
                        <AddElement
                            text={this.state.categoryTextValue}
                            onAdd={() => this.onCategoryAdd()}
                            onChange={(event) => this.setState({ categoryTextValue: event.target.value })}
                            placeholder="Enter category title"/>
                    </Col>
                    <Col xs={12} sm={8} className="text-right">
                        { this.isListVisible() &&
                        <AddElement
                            text={this.state.todoTextValue}
                            onAdd={() => this.onAddTodo()}
                            onChange={(event) => this.setState({ todoTextValue: event.target.value })}
                            placeholder="Add ToDo"/>
                        }
                    </Col>
                </Row>

                <Row>
                    <Col xs={12} sm={4}>
                        <CategoryList
                            onSave={(category) => this.props.saveCategory(category)}
                            // TODO change path if deleted selected category
                            onDelete={(id) =>  this.onCategoryDelete(id) }
                            categories={ this.props.categories }/>
                    </Col>
                    <Col xs={12} sm={8}>
                        <UndoRedo />
                        { this.isListVisible() &&
                        <ToDoList
                            onToggle={ (todo) => this.props.saveTodo(todo) }
                            items={ this.props.todos }/>
                        }
                    </Col>
                </Row>
            </Grid>
        );
    }
}

Home.contextTypes = {
    router: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
    const urlFilterQuery = new URLSearchParams(ownProps.location.search).get('filter');
    const showDone = state.filter.showDone;
    const query = (urlFilterQuery || state.filter.query).toLowerCase();

    const todos = state.todos.present.filter(todo => {
        return todo.category === ownProps.match.params.id
        && !(!showDone && todo.isDone) // filter done
        && (!query || (query && todo.text.toLowerCase().includes(query) ));
    } );

    return {
        todos: todos,
        categories: parseCategory(state.categories),
        filter: { ...state.filter, query },
        progress: countProgress(state.todos.present)
    };
}

function mapDispatchToProps(dispatch) {
    return {
        saveCategory: category => dispatch(saveCategory(category)),
        deleteCategory: id => dispatch(deleteCategory(id)),
        saveTodo: todo => dispatch(saveTodo(todo)),
        toggleShowDone: () => dispatch(toggleShowDone()),
        changeFilter: (query) => dispatch(changeFilter(query))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);