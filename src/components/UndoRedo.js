import React from 'react';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/lib/Button';
import { undo, redo } from '../store/actions'

let UndoRedo = ({ canUndo, canRedo, onUndo, onRedo }) => (
    <p className="text-right">
        <Button onClick={onUndo} disabled={!canUndo}>
            Undo
        </Button>
        <Button onClick={onRedo} disabled={!canRedo}>
            Redo
        </Button>
    </p>
)

const mapStateToProps = (state) => ({
    canUndo: state.todos.past.length > 0,
    canRedo: state.todos.future.length > 0
})

const mapDispatchToProps = (dispatch) => {
    return {
        onUndo: () => dispatch(undo()),
        onRedo: () => dispatch(redo()),
    };
}

UndoRedo = connect(
    mapStateToProps,
    mapDispatchToProps
)(UndoRedo)

export default UndoRedo