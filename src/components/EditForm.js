import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';

class EditForm extends Component {
    render() {
        const { handleSubmit, onCancel } = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <div className="text-right">
                    <Button type="submit">Save changes</Button>
                    <Button onClick={ onCancel }>Cancel</Button>
                </div>
                <FormGroup>
                    <ControlLabel>Text</ControlLabel>
                    <Field name="text" className="form-control" component="input" type="text"/>
                </FormGroup>
                <FormGroup className="checkbox">
                    <ControlLabel><Field name="isDone" component="input" type="checkbox"/> Done</ControlLabel>
                </FormGroup>
                <FormGroup controlId="formControlsTextarea">
                    <ControlLabel>Textarea</ControlLabel>
                    <Field name="description" className="form-control" component="textarea"/>
                </FormGroup>
            </form>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        initialValues: {
            text: ownProps.todo.text,
            isDone: ownProps.todo.isDone,
            description: ownProps.todo.description
        }
    };
}
let ReduxEditForm = reduxForm({ form: 'edit' })(EditForm);
export default connect(mapStateToProps)(ReduxEditForm);
