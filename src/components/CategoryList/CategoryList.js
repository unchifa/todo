import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';
import TiArrowSortedDown from 'react-icons/lib/ti/arrow-sorted-down';
import FaLongArrowLeft from 'react-icons/lib/fa/mail-reply';
import TiTrash from 'react-icons/lib/ti/trash';
import TiPlus from 'react-icons/lib/ti/plus';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Confirm from 'react-confirm-bootstrap';
import './CategoryList.css';
import EditableField from '../Shared/EditableField';

class CategoryList extends Component {
    state = { selectedCategoryId: null };

    onAdd(category) {
        this.props.onSave(category);
        this.setState({ selectedCategoryId: null })
    }

    selectedClass(id) {
        return this.context.router.route.match.params.id ===  id ?  ' selected' : '';
    }

    renderList(categories) {
        return (
            <ul className="list-unstyled category-list">
                { categories.map(item => this.props.editMode
                    ? this.renderItemEditMode(item)
                    : this.renderItem(item))}
            </ul>
        );
    }

    renderItemEditMode(item) {
        return (
            <li key={ item.id }>
                <Row className="category-item">
                    <Col xs={6}>
                        { item.text }
                    </Col>
                    <Col xs={6} className="text-right">
                        { this.props.selectedCategory !== item.id
                            ? <Button
                                onClick={(e) => this.props.onMove(item.id) }
                                bsStyle="link">
                                <FaLongArrowLeft />
                            </Button>
                            : <span>Move here</span>
                        }
                    </Col>
                </Row>
                { item.children && this.renderList(item.children) }
            </li>
        );
    }

    renderItem(item) {
        return (
            <li key={ item.id }>
                <div className={"category-item" + (this.selectedClass(item.id))}>
                    { item.children && <TiArrowSortedDown className="item-arrow"/> }
                    <span className="item-text">
                        <EditableField
                            onSave={ (text) => this.props.onSave({ text, id: item.id }) }
                            text={ item.text }
                            link={  `/category/${ item.id }` }/>
                    </span>
                    <span className="item-control">
                        <Confirm
                            onConfirm={ () => this.props.onDelete(item.id) }
                            body={ `Are you sure you want to delete ${ item.text }?` }
                            confirmText="Delete"
                            title="Delete category">
                            <Button bsStyle="link"><TiTrash /></Button>
                        </Confirm>
                        <Button onClick={ () => this.setState({ selectedCategoryId: item.id }) } bsStyle="link">
                            <TiPlus />
                        </Button>
                    </span>
                    { this.state.selectedCategoryId === item.id && <EditableField
                        createNew
                        onSave={ (text) => this.onAdd({ text, parent: item.id }) }
                        text={ 'New category' }/> }
                </div>

                { item.children && this.renderList(item.children) }
            </li>
        );
    }

    render() {
        return (
            <div className="categories">
                { this.renderList(this.props.categories) }
            </div>
        );
    }
}

CategoryList.contextTypes = {
    router: PropTypes.object.isRequired
}

export default CategoryList;
